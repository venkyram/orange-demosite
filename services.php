<!DOCTYPE html>
<html>

<head>
    <title>Services | Orange</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
</head>

<body>
    <header>
        <div class="navbar container-fluid sticky-one align-vertical">
            
            <div class="col-md-12 col-xs-12 col-sm-12 logo-wrapper text-center">
                <a href="/" class="logo"><img class="img-responsive" src="images/orange-logo.png"></a>
            </div>
        
        </div>
    </header>
    <div class="phone-contact1">
       <span class="phone-icon"><img src="images/contact.svg"></span>
       <span class="phone-no"><a href="tel:+918807088076">+91 88070 88076</a></span>
     </div>
    <div class="header-offset"></div>
  
   

    <section class="container-fluid services padZero hidden-xs" id="services">
        
        <div class="container padZero">
            <div class="col-md-12 menu-title padZero col-xs-12">
                <h1>Our Services</h1>
            </div>
            <div class="col-md-3 col-sm-3 padZero menu-service col-xs-12">
                <h4>Category</h4>
                <ul class="service-names menu-names padZero magZero">
                    
                    
                </ul>
             </div>
            <div class="col-md-9 col-sm-9 padZero col-xs-12">
                <div class="menu-box">
                    <ul class="service-content">
                     
                        
                          
                    </ul>
                </div>
            </div>
        </div>
    </section>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>

</html>
