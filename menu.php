<!DOCTYPE html>
<html>

<head>
    <title>Menu | Orange Salon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <meta property="og:title" content="Orange Salons" />
    <meta property="og:site_name" content="Orange Premium Discount Salon" />
    <meta propety="og:url" content="https://menu.orangesalons.com/" />
    <meta property="og:image" content="https://orangesalons.com/images/og-image.jpg" />
    <meta property="og:description" content="Orange is your exquisite neighbourhood haven of styling and pampering. We carry the finest beauty products and modish stylists to offer the largest selection of beauty services in the city. You get the extraordinary bit of serendipity in availing discounts on every visit. Orange promises to make premium services highly affordable, now & forever."/>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@TheOrangeSalons">
    <meta name="twitter:title" content="Orange Salons" />
    <meta name="twitter:image" content="https://orangesalons.com/images/og-image.jpg" />
    <meta name="twitter:description" content="Orange is your exquisite neighbourhood haven of styling and pampering. We carry the finest beauty products and modish stylists to offer the largest selection of beauty services in the city. You get the extraordinary bit of serendipity in availing discounts on every visit. Orange promises to make premium services highly affordable, now & forever.">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
    <style type="text/css">
        .height-top
        {
            height: 93vh;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
                    align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
                    justify-content: center;
        }
    </style>
</head>

<body>
    <header>
        <div class="navbar container-fluid sticky-one align-vertical">
        <div style="color: black; font-size: 15px; padding: 15px; display: inline-block;margin-left: 10px; position: absolute; z-index: 9999;"><a href="i-pad.php"><img src="images/left.svg"></a></div>
            <div class="col-md-12 col-xs-12 col-sm-12 logo-wrapper text-center">
                <a href="/" class="logo"><img class="img-responsive" src="images/orange-logo.png"></a>
            </div>
        </div>
    </header>
    
    <div class="header-offset"></div>
  <section class="container-fluid  padZero hidden-xs height-top" id="services">
        <div class="container padZero">
            <div class="col-md-8 col-sm-6 service-title padZero col-xs-6">
                <h1>Our Services</h1>
            </div>
        <div class="col-md-4 col-sm-6 text-center" style="padding-top: 10px;">
               
        <div class="form-group input-group home-search">
          <input type="input" class="form-control txt-search" id="txt-search" placeholder="Start typing your service..." autocomplete="off">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
              
            </div>

            <div class ="filter-records col-md-12 col-sm-12">
                
            </div>
            <div class="search-hide col-md-12">

            <div class="col-md-3 col-sm-3 padZero menu-service col-xs-12">
                <h4>Category</h4>
                <ul class="service-names menu-names padZero magZero">
                    
                    
                </ul>
             </div>
            <div class="col-md-9 col-sm-9 padZero col-xs-12">
                <div class="menu-box">
                    <a href="contact.php" data-id="faq" class="faq-link scroll-link">Frequently Asked Questions</a>
                    <ul class="service-content">
                     
                        
                          
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="container-fluid services padZero hidden-sm hidden-md hidden-lg mobile_top" id="ser-scroll" >
<div class="search-services">
    <div class="close-btn text-right">
        <i class="fa fa-times fa-2x"></i>
    </div>
    <div class="container box-width">
    <form role="form">
        <div class="form-group">
          <input type="input" class="form-control input-lg txt-search" id="txt-search" placeholder="Start typing your service..." autocomplete="off">
        </div>
    </form>
    <div class ="filter-records"></div>
  </div>
</div>
      <div class="container">
        <div class="service-title padZero col-xs-10">
                <h1>Our Services</h1>
            </div>
            <div class="col-xs-2 search-pop text-center">
                <i class="fa fa-search fa-2x"></i>
            </div>
            <div class="padZero list-servicemob col-xs-12">
                <!-- <div class="input-group home-search" style="margin-bottom: 10px;">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                <input id="searchInputMob" class="search form-control" placeholder="Search"/>
            </div> -->

                <ul class="servicename-mobile" >
                    
                    
                </ul>
            </div>
            <div class=" service-mobile active padZero col-xs-12">
                <img src="images/left.svg" class="backTo">
              <ul class=" padZero magZero">
                    </ul>
            </div>
      </div>
    </section>
    
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript">
   
   $(document).ready(function(){
document.addEventListener("showkeyboard", function(){ keyboardOpen();}, false);
document.addEventListener("hidekeyboard", function(){ keyboardClose();}, false);
    
   });
   function keyboardOpen()
   {
    alert("open");

   }
   function keyboardClose()
   {
     alert("close");
   }
</script>

</html>
