 <link rel="stylesheet" type="text/css" href="css/tagit.css">
 <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
<div id="book" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->

                    <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="modal-body">
                            <h3 class="modal-title">Book Appointment</h3>
                            <span id="spnPhoneStatus"></span>
                    <div class="col-md-6 book_div  hidden-xs hidden-sm">
                        <div class="col-md-6">
                            <h4>Category</h4>

                        </div>
                        <div class="col-md-6">
                            <h4>Sub-Category</h4>
                        </div>
                <div class="col-md-6 book-app col-xs-12">
                  <ul class="service-book padZero magZero">
                </ul>
            </div>
            <div class="col-md-6 padZero">
                <ul class="book-box padZero magZero">
                </ul>
            </div>
                    </div>
                        <div class="col-md-6 padZero form">
                            <h4>Please Fill The form..</h4>
                            <form role="form" method="post" id="book-appointment" >

                                <div class="form-group col-md-12 padZero input-group">
                                    <span class="input-group-addon"><img src="images/Calender.svg"></span>
                                    <input type="text" class="form-control" id="datetime" required maxlength="50" name="date" placeholder="Date & Time" readonly="true">

                                </div>
                                <div class="form-group col-md-12 input-group">

                                    <span class="input-group-addon">+91</span>
                                    <input type="phone" class="form-control phone" name="phone"  id="phone"  required maxlength="10" placeholder="Mobile Number">
                                </div>
                                <div class="form-group col-md-12 padZero">
                                <select class="form-control" name="location-salon" required="" aria-required="true" id="select">
                                      <option class="selected" value=""><a href="#">Location</a></option>
                                      <option value="Avadi"><a href="#">Avadi</a></option>
                                      <option value="Thiruninravur"><a href="#">Thiruninravur</a></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 padZero visible-xs hidden-md visible-sm" style="height: 33px;">
                                    <div class="col-md-12 col-sm-6 col-xs-6 category padLeft" style="padding-bottom: 10px;">
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-6 subcategory padRight" style="padding-bottom: 10px;">
                                    </div>
                                </div>
                                 <div class="form-group col-md-12 padZero">
                                    <input name="services" class="selectedService" class="selected-tags form-control" value="" aria-required="true" required="">
                            </div>
                                <div class="form-group col-md-12 conta contact-popup">
                                <div class="g-recaptcha" data-sitekey="6Lf6TkEUAAAAAKB_6sMHqjS5BJKe-O9I6Me01tkY"></div>
                                </div>
                             <div class="form-group col-md-12 text-right magZero">
                                <button type="submit" class="submit-btn bg-black" id="btnContactUs">BOOK NOW</button>
                            </div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
