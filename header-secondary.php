<header>
        <div class="navbar container-fluid sticky-one align-vertical">
            <div class="col-md-2 col-xs-10 logo-wrapper">
                <a href="/" class="logo"><img class="img-responsive" src="images/orange-logo.png"></a>
            </div>
            <div class="col-md-10 col-sm-12 text-right hidden-xs visible-lg visible-md hidden-sm padZero">
                <nav class="desktop-menu">
                    <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php" data-id="about" class="scroll-link">About Us</a></li>
                    <li><a href="index.php" data-id="services" class="scroll-link">Services</a></li>
                   <!--  <li><a href="index.php" data-id="brands" class="scroll-link">Brands</a></li> -->
                    <li class="blog-menu"><a href="blog.php">Blog</a></li>
                    <li class="contact-menu"><a href="contact.php">Contact Us</a></li>
                    <li class="franchise-menu"><a href="franchise.php" class="franchise-btn">Franchise</a></li>
                    <!--  <li><a href="" data-toggle="modal" data-target="#model" class="orange-btn bg-black">Be a Model</a></li> -->
                    <!-- <li><a href="" class="orange-btn bg-black" data-toggle="modal" data-target="#book">Book Appointment</a></li> -->
                    </ul>
                </nav>
            </div>
            <div class=" visible-xs hidden-lg hidden-md visible-sm mobile-menu-wrapper
            ">
                <i class="expand-menu fa fa-bars"></i>
                <nav class="mobile-menu" style="display: none">
                    <ul>
                         <li><a href="index.php">Home</a></li>
                        <li><a href="index.php" data-id="about" class="scroll-link">About Us</a></li>
                        <li><a href="index.php" data-id="ser-scroll" class="scroll-link">Services</a></li>
                        <li><a href="index.php" data-id="brands" class="scroll-link">Brands</a></li>
                        <li><a href="blog.php">Blog</a></li>
                       <li class="contact-menu"><a href="index.php"  data-id="contact" class="scroll-link">Contact Us</a></li>
                        <li><a href="franchise.php">Franchise</a></li>
                       <!--  <li><a href="" data-toggle="modal" data-target="#model">Be a Model</a></li> -->
                       <!--  <li><a href="" data-toggle="modal" data-target="#book">Book Appointment</a></li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    
    

     