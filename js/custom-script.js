var globalArray = [];
var data = '';
var res = "";
$(document).ready(function() {

    $(document).on('click', '.expand-menu', function() {
        $(this).removeClass('expand-menu');
        $(this).removeClass('fa fa-bars');
        $(this).addClass('collapse-menu');
        $(this).addClass('fa fa-close');
        $(this).siblings('nav').slideDown();
    });

    $(document).on('click', '.collapse-menu', function() {
        $(this).removeClass('collapse-menu');
        $(this).removeClass('fa fa-close');
        $(this).addClass('expand-menu');
        $(this).addClass('fa fa-bars');
        $(this).siblings('nav').slideUp();
    });
    $(".mobile-menu li").click(function() {
        $(this).parent().parent().siblings().removeClass('collapse-menu');
        $(this).parent().parent().siblings().removeClass('fa fa-close');
        $(this).parent().parent().siblings().addClass('expand-menu');
        $(this).parent().parent().siblings().addClass('fa fa-bars');
        $(this).parent().parent().slideUp();

    });

    var landingHeight = $(".landing-page").height() - 80;
    var stickyHeight = landingHeight;
    var yourNavigation = $(".navbar");

    $(window).scroll(function() {
        if ($(this).scrollTop() > stickyHeight) {
            yourNavigation.addClass("sticky");;
        } else {
            yourNavigation.removeClass("sticky");

        }
    });
    $(".phone-contact").click(function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
        }

    });
    $(".phone-contact1").click(function() {


        if ($(this).hasClass("active")) {

            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
        }

    });
    $.ajax({
        type: "GET",
        url: "https://store.orangesalons.com/get_menu",
        cache: false,
        success: function(data) {
            var list = JSON.stringify(data.data);
            window.localStorage.setItem("service-list", list);
            getMenuContent(data.data);
            getPriceContent(data.data);
            getMenulist(data.data);
            getSearchMenu(data.data);
        },
        error: function(data) {
            console.log(data);
        }
    });

    function getSearchMenu(data) {

        $('.txt-search').keyup(function() {

            var output = '';
            var searchField = $(this).val();
            if (searchField === '') {
                $('.filter-records').html('');
                return;
            }
            output += '<div class="box-width">';
            var regex = new RegExp(searchField, "i");
            var values = '';
            $.each(data, function(key, val) {
                val.forEach(function(value) { values = Object.keys(value)[0] });
                if (key.search(regex) != -1) {
                    output += '<div class="flex-content"><h5 class="col-md-3 col-xs-12 text-center padLeft">' + key + '</h5>';
                    output += "<div class='col-md-9 col-xs-12 mobPad'><div class='col-md-12 tableHead padZero'>"
                    output += "<div class=' one'> SERVICES NAME </div>"
                   /* output += "<div class='col-md-1 col-xs-1 padZero text-center'> 50% </div>"
                    output += "<div class='col-md-1 col-xs-2 padZero text-center'> 40% </div>"*/
                    if ((key != "Hair Do" ) && (key != "Make up") && (key != "Saree Drapping"))
                {
                    output += "<div class=' padZero text-center two'> 50% </div>"
                    output += "<div class=' padZero text-center three' style='font-size:36px;'> - </div>";
                    output += "<div class=' padZero text-center four'> 15% </div>"
                }
                    output += "<div class=' padZero text-center five'> PRICE </div>"
                    output += "</div>"
                    val.forEach(function(value) {
                        var key1 = Object.keys(value);
                        output += '<div class="col-md-12 tableBody padZero">';
                        output += '<div class="one">' + key1 + '</div>';
                       /* output += '<div class="col-md-1 col-xs-1 padZero text-center">₹' + Math.round(value[key1][6]) + '</div>';
                        output += '<div class="col-md-1 col-xs-2 padZero text-center">₹' + Math.round(value[key1][5]) + '</div>';*/
                        if ((key != "Hair Do" ) && (key != "Make up") && (key != "Saree Drapping"))
                    {
                        output += '<div class="two padZero text-center">₹' + Math.round(value[key1][4]) + '</div>';
                        output += '<div class="three padZero text-center" style="font-size:36px;">-</div>';
                        output += '<div class="four padZero text-center">₹' + Math.round(value[key1][3]) + '</div>';
                    }
                        output += '<div class="five padZero text-center">₹' + Math.round(value[key1][2]) + '</div>';
                         /*var description = value[key1][1]
        if ((description != null) && (description != "")){
          output += '<p class="search-p">' +value[key1][1]+ '</p>';
        }*/
                       
                        output += '</div>';
                        output += "<div class='clear'></div>";

                    });
                    output += '</div></div>'
                } else {
                    val.forEach(function(value) {
                        key2 = Object.keys(value);
                        if (key2[0].search(regex) != -1) {
                            output += '<div class="flex-content"><h5 class="col-md-3 col-xs-12 text-center padLeft">' + key + '</h5>';
                            output += "<div class='col-md-9 col-xs-12 mobPad'><div class='col-md-12 tableHead padZero'>"
                            output += "<div class='one'> SERVICES NAME </div>"
                           /* output += "<div class='col-md-1 col-xs-1 padZero text-center'> 50% </div>"
                            output += "<div class='col-md-1 col-xs-2 padZero text-center'> 40% </div>"*/
                            if ((key != "Hair Do" ) && (key != "Make up") && (key != "Saree Drapping"))
                        { 
                            output += "<div class='two padZero text-center'> 50% </div>"
                            output += "<div class='three padZero text-center' style='font-size:36px;'> - </div>"
                            output += "<div class='four padZero text-center'> 15% </div>"
                        }
                            output += "<div class='five padZero text-center'> PRICE </div>"
                            output += "</div>"
                            output += '<div class="col-md-12 mobPad tableBody padZero">'
                            output += '<div class="one">' + key2 + '</div>';
                           /* output += '<div class="col-md-1 col-xs-1 padZero text-center">₹' + Math.round(value[key2][6]) + '</div>';
                            output += '<div class="col-md-1 col-xs-2 padZero text-center">₹' + Math.round(value[key2][5]) + '</div>';*/
                            if ((key != "Hair Do" ) && (key != "Make up") && (key != "Saree Drapping"))
                         {
                            output += '<div class="two padZero text-center">₹' + Math.round(value[key2][4]) + '</div>';
                             output += '<div class="three padZero text-center" style="font-size:36px;">-</div>';
                            output += '<div class="four padZero text-center">₹' + Math.round(value[key2][3]) + '</div>';
                         }
                            output += '<div class="five padZero text-center">₹' + Math.round(value[key2][2]) + '</div>';
                          /* var description = value[key2][1]
        if ((description != null) && (description != "")){
          output += '<p class="search-p">' +value[key2][1]+ '</p>';
        }*/
                            output += '</div>';
                            output += "<div class='clear'></div>";
                            output += '</div></div>'
                        }
                    });


                }
            });


            output += '</div>';
            $('.filter-records').html(output);
        });
    }

    function getMenuContent(data) {
        var services = new Array();
        var categories = new Array();
        var subcat = new Array();
        for (var category in data) {
            categories.push(category);
        }
        var categories_list = '';
        var category_service = '';
        category_service += "<select class='form-control categoryMenu' onchange='showSubMenu(this.options[this.selectedIndex].text)'>"
        categories.forEach(function(value, index) {
            categories_list += "<li onclick='showSubMenu(this)'>" + value + "</li>";
            category_service += "<option>" + value + "</option>";
        });
        category_service += "</select>"

        $(".service-book").html(categories_list);
        $(".category").html(category_service);
        showSubMenu(categories[0]);
        $(".categoryMenu").select2({
            width: '100%'
        });
        $(".service-book li").first().addClass("active");
    }

    function getPriceContent(data) {
        var services = new Array();
        var categories = new Array();
        var subcat = new Array();
        for (var category in data) {
            categories.push(category);
        }
        var categories_list = '';
        /*categories_list += "<h4>Category</h4>";*/
        categories.forEach(function(value, index) {
            categories_list += "<li id=" + index + " class='menus' onclick='showMenuPrice(this)'><img class='hidden-md hidden-lg' src='images/right.svg'>" + value + "</li>";
        });

        showMenuPrice(categories[0]);

        $(".service-names").html(categories_list);
        $(".servicename-mobile").html(categories_list);
        $(".service-names li").first().addClass("active");
        $(".service-mobile").removeClass("active");

    }

    function getMenulist(data) {
        var services = new Array();
        var categories = new Array();
        var subcat = new Array();
        for (var category in data) {
            categories.push(category);
        }
        var categories_list = '';
        categories_list += "<h4>Category</h4>";
        categories.forEach(function(value, index) {
            categories_list += "<li id = " + index + " class='listService'>" + value + "</li>";
            showListPrice(value, index);

        });

        /*showMenuPrice(categories[0]);*/
        $(".service-names1").html(categories_list);
        $('.selectedService').tagit({
            autocomplete: {
                source: globalArray
            }
        });
        $(".tagit-new input").attr({
            placeholder: "Select or Type to Add Service",
            name: "tags"
        });

    }

    $('.backTo').click(function() {
        $('.service-mobile').removeClass("active");
    });

    $('.search-pop i').click(function() {
        $('.search-services').addClass("open");
        $("body").css("overflow", "hidden");
        $('.txt-search').val("");
        $('.filter-records').html('')

    });
    $('.close-btn i').click(function() {
        $('.search-services').removeClass("open");
        $("body").css({ "overflow-y": "auto", "overflow-x": "hidden" });

    });
    var scroll = $.cookie('scroll');
    if (scroll) {
        scrollToID(scroll, 1000);
        $.removeCookie('scroll');
    }
    $('.scroll-link').click(function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var href = $(this).attr('href');
        if (href === '#') {
            scrollToID(id, 1000);
        } else {
            $.cookie('scroll', id);
            window.location.href = href;
        }
    });

    function scrollToID(id, speed) {
        var offSet = 70;
        var obj = $('#' + id).offset();
        var targetOffset = obj.top - offSet;
        $('html,body').animate({ scrollTop: targetOffset }, speed);
    }


    /* validations */


    $('#phone').blur(function(e) {
        if (validatePhone('phone')) {
            $('#spnPhoneStatus').html('');
            $('#spnPhoneStatus').css('color', 'green');
        } else {
            $('#spnPhoneStatus').html('Please Enter valid Mobile Number');
            $('#spnPhoneStatus').css('color', 'red');
        }
    });


    function validatePhone(phone) {
        var a = $('#phone').val();
        var filter = /^\d{10}$/;
        if (filter.test(a)) {
            return true;
        } else {
            return false;
        }


    }

    $('#phone_franchise').blur(function(e) {
        if (validatePhoneNo('phone')) {
            $('#spnPhoneNo').html('');
            $('#spnPhoneNo').css('color', 'green');
        } else {
            $('#spnPhoneNo').html('Please Enter valid Mobile Number');
            $('#spnPhoneNo').css('color', 'red');
        }
    });

    function validatePhoneNo(phone) {
        var a = $('#phone_franchise').val();


        var filter = /^\d{10}$/;
        if (filter.test(a)) {
            return true;
        } else {
            return false;
        }


    }
    $('#phone_career').blur(function(e) {
        if (validate('phone')) {
            $('#spnPhoneNoCareer').html('');
            $('#spnPhoneNoCareer').css('color', 'green');
        } else {
            $('#spnPhoneNoCareer').html('Please Enter valid Mobile Number');
            $('#spnPhoneNoCareer').css('color', 'red');
        }
    });

    function validate(phone) {
        var a = $('##phone_career').val();


        var filter = /^\d{10}$/;
        if (filter.test(a)) {
            return true;
        } else {
            return false;
        }
      }

    $('#datetime').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        minDate: moment(new Date()).add(1, 'days')
    });


    $('.txt-search').keyup(function() {
        if ($(this).val() != "") {
            $(".filter-records").show();
            $(".search-hide").hide();
        } else {
            $(".filter-records").hide();
            $(".search-hide").show();
        }

    });


    /*$('.txt-search').focus(function() {
       $(".filter-records").show();
       $(".search-hide").hide();  
    }); */
    $(".img-iframe").hover(
        function() {
            $(this).children().addClass("active")
        },
        function() {
            $(this).children().removeClass("active");
        }
    );

    /*$(document).ready(function() {
        $('#book-appointment').bind('submit', function() {

            var data = [];
            data["online_appointment"] = [];
            data["online_appointment"]["services"] = [];
            data["online_appointment"]["phone_number"] = $("input[name='phone']").val();
            data["online_appointment"]["start_time"] = $("input[name='date']").val();
            var services = $("input[name='services']").val();
            data["online_appointment"]["location"] = $("select[name='location-salon']").val();
            var servicesArray = services.split(",")
            servicesArray.forEach(function(value) {
                var e = {};
                var s = value.split("(");
                var g = s[0].split("₹");
                var service_name = g[0].substring(0, g[0].length - 3);
                var group_name = s[1].substring(0, s[1].length - 1);
                e.group_name = group_name;
                e.service_name = service_name;
                data["online_appointment"]["services"].push(e);
            });

            debugger;
            $.ajax({
                type: 'post',
                url: 'http://35.193.250.3/book_online',
                data: $('form').serialize(),
                error: function() {
                    var html = "<span class='book-text'><i class='material-icons'>error</i></br>There was a error in submitting your form. Kindly reload this page.</span>"
                    $("#book-appointment").html(html);
                },
                success: function() {
                    var html = "<span class='book-text'><i class='material-icons'>done</i></br>Thanks for getting in touch with us.</br> Our franchise executive will get back to you ASAP.</span>"
                    $("#book-appointment").html(html);

                }
            });
            return false;
        });
    });
*/


    $(".formsubmit").submit(function(e) {
        if (res.length === 0) {
            $('.msg-error').text("reCAPTCHA is mandatory");
            e.preventDefault();
        } else {
            $('.msg-error').text('');

            $.ajax({
                type: "POST",
                url: "save.php",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $("#loading-image").show();
                },
                success: function() {
                    $("#loading-image").hide();
                    $(".success").css("display", "block");
                    jQuery('html,body').animate({ scrollTop: 0 }, 0);
                    $(".formsubmit").trigger("reset");
                    grecaptcha.reset();
                    setTimeout(HideData, 10000);
                    res = "";

                },
                error: function() {
                    $(".error").css("display", "block");
                    $("#loading-image").hide();
                    setTimeout(HideData, 10000);

                }
            });
            e.preventDefault();
        }
    });

    function HideData() {
        $('.message').slideUp();
        $('.alert').slideUp();
    }

});

var onloadCallback = function() {
    if ($('#recaptcha2').length) {
        grecaptcha.render('recaptcha2', {
            'sitekey': '6Lf6TkEUAAAAAKB_6sMHqjS5BJKe-O9I6Me01tkY',
            'callback': verifyCallback
        });
    }
    if ($('#recaptcha1').length) {
        grecaptcha.render('recaptcha1', {
            'sitekey': '6Lf6TkEUAAAAAKB_6sMHqjS5BJKe-O9I6Me01tkY',
            'callback': verifyCallback
        });
    }
};
var verifyCallback = function(response) {
    res = response;
};

function showMenuPrice(category) {
    $(".servicename-mobile li").removeClass("active");
    $(".service-names li").removeClass("active");
    /*$(".service-names li").first().addClass("active");
    $(".servicename-mobile li").first().addClass("active");*/

    $(category).addClass("active");
    var list = JSON.parse(window.localStorage.getItem("service-list"));
    var toAdd = '';
    if (typeof category === 'string') {
        toAdd += '<h4>' + category + '</h4>'
        subcat = list[category];
    } else {
        toAdd += '<h4>' + category.innerText + '</h4>'
        subcat = list[category.innerText];
    }
    if (category.innerText == "Hair Cut - Advanced") {
        toAdd += "<p class='include'>(Includes Wash & Blow Dry)</p>";
    }
    toAdd += '<li>';
    toAdd += "<div class='list-table'><div class='heading-row'>"
    toAdd += "<div class='col-heading-1'> SERVICES NAME </div>"
   /* toAdd += "<div class='col-heading-2'> 50% </div>"
    toAdd += "<div class='col-heading-3'> 40% </div>"*/
    if ((category.innerText != "Hair Do" ) && (category.innerText != "Make up") && (category.innerText != "Saree Drapping"))
    {
    toAdd += "<div class='col-heading-4'> 50% </div>"
    toAdd += "<div class='col-heading-3'> - </div>"
    toAdd += "<div class='col-heading-5'> 15% </div>"
    }
    toAdd += "<div class='col-heading-6'> PRICE </div>"
    toAdd += "</div><div class='clear'></div>"
    subcat.forEach(function(value) {
        var key = Object.keys(value);
        toAdd += "<div class='detail' onclick='showDescription(this)'>";
        toAdd += "<div class='col-1'>" + key + "</div>";
       /* toAdd += "<div class='col-2'>₹" + Math.round(value[key][6]) + "</div>";
        toAdd += "<div class='col-3'>₹" + Math.round(value[key][5]) + "</div>";*/
        if ((category.innerText != "Hair Do") && (category.innerText != "Make up") && (category.innerText != "Saree Drapping"))
    {
        toAdd += "<div class='col-4'>₹" + Math.round(value[key][4]) + "</div>";
        toAdd += "<div class='col-3'>-</div>";
        toAdd += "<div class='col-5'>₹" + Math.round(value[key][3]) + "</div>";
    }
        toAdd += "<div class='col-6'>₹" + value[key][2] + "</div>";
        /*var description = value[key][1]
        if ((description != null) && (description != "")){
         toAdd += "<p class='discription'>" + value[key][1] + "</p>";
        }*/
        toAdd += "</div> <div class='clear'></div>";
    });
    toAdd += "</div>";
    toAdd += "</li>";

    $(".service-content").html(toAdd);
    $(".service-mobile ul").html(toAdd);
    $(".service-mobile").toggleClass("active");
    $(".detail>p").hide();
    $(".detail>p").first().show();
}

function showListPrice(category, index) {

    $(category).addClass("active");
    var list = JSON.parse(window.localStorage.getItem("service-list"));
    var toAdd = '';
    var categoryname;
    var keys = '';
    if (typeof category === 'string') {
        toAdd += '<h4>' + category + '</h4>'
        categoryname = category;
        subcat = list[category];
    } else {
        categoryname = category.innerText;
        toAdd += '<h4>' + category.innerText + '</h4>'
        subcat = list[category.innerText];
    }
    toAdd += '<li id=' + index + ' name=' + category + '>';
    toAdd += "<table><thead>"
    toAdd += "<th> SERVICES NAME </th>"
    toAdd += "<th> 20% </th>"
    toAdd += "<th> 30% </th>"
    toAdd += "<th> 40% </th>"
    toAdd += "<th> 50% </th>"
    toAdd += "<th> PRICE </th>"
    toAdd += "</thead>"
    subcat.forEach(function(value) {

        var key = Object.keys(value);
        keys = key;
        toAdd += "<tr>";
        toAdd += "<td>" + key + "</td>";
        toAdd += "<td>₹" + Math.round(value[key][1]) + "</td>";
        toAdd += "<td>₹" + Math.round(value[key][2]) + "</td>";
        toAdd += "<td>₹" + Math.round(value[key][3]) + "</td>";
        toAdd += "<td>₹" + Math.round(value[key][4]) + "</td>";
        toAdd += "<td>₹" + value[key][0] + "</td>";
        toAdd += "</tr>";
        globalArray.push(categoryname + "-" + keys + " - ₹" + value[key][0]);
    });
    toAdd += "</table></li>"
    $(".service-content1").append(toAdd);
}

function showSubMenu(category) {
    $(".service-book li").removeClass("active");
    $(category).addClass("active");
    var list = JSON.parse(window.localStorage.getItem("service-list"));
    var categoryname;
    if (typeof category === 'string') {
        categoryname = category;
        subcat = list[category];
    } else {
        categoryname = category.innerText;
        subcat = list[category.innerText];
    }
    var toAdd = '';
    var toOption = '';
    toOption = "<select class='form-control subCategory' onchange='getSubMenuTag(this.options[this.selectedIndex])'>"
    subcat.forEach(function(value) {
        var key = Object.keys(value);
        toAdd += "<li onclick='getSubMenuTag(this)' category=" + categoryname + ">" + key + " - ₹" + value[key][0] + "</li>";
        toOption += "<option category=" + categoryname + ">" + key + " - ₹" + value[key][0] + "</option>";

    });
    toOption += "</select>"
    $(".book-box").html(toAdd);
    $(".subcategory").html(toOption);
    $('.subCategory').select2({
        width: '100%'
    });
}

function getSubMenuTag(element) {

    var category = element.getAttribute("category");
    var tagData = element.innerText;
    $('.selectedService').tagit('createTag', tagData + "(" + category + ")");
}

function showDescription(des) {
    $(".discription").hide();
    var head = des.getElementsByTagName("p");
    $(head).show();
}