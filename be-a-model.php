


<div id="model" class="modal fade" role="dialog">
    <div id="loading-image" style="display: none;">
                    <img src="images/orange-loader.gif">
                </div>
                <div class="modal-dialog ">
                    <!-- Modal content-->
                    <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="modal-body">
                            <h3 class="modal-title">Want be a Model</h3>
            <div class="form-container">
                <div class="text-center">
                    <p class="message success" style="display: none;">
                     Thanks for getting in touch. We will get back to you shortly.
                   </p>
                   <p class="message error" style="display: none;">
                    Sorry! Your form wasn't submitted. Kindly try again.
                   </p>
                </div>
        <form role="form" method="post" enctype="multipart/form-data" class="formsubmit">
            <input type="hidden" name="form_type" id="form_type" value="be_a_model">
            <div class="form-group col-md-12">
                <input type="text" class="form-control" id="name" name="fullname" required maxlength="50" placeholder="Full Name">
            </div>
            <div class="form-group col-md-12">
                <input type="text" name="dob" id="dob" class="form-control" required 
                placeholder="Date of Birth">
            </div>
            <div class="form-group col-md-12">
                <input type="tel" class="form-control" id="phone" name="phone" required maxlength="10" placeholder="Mobile Number" pattern="[\+\s0-9]{10}">
            </div>
            
            <div class="form-group col-md-12">
                <input type="email" class="form-control" id="emp" name="email" required maxlength="50" placeholder="Email">
            </div>
            <div class="form-group col-md-6 input-group" style="padding: 0px 15px !important;">
                <span class="input-group-addon" style="border: 1px solid;">@</span>
               <input type="text" class="form-control" name="insta" maxlength="50" placeholder="Instagram ID" style="border-left: 0px;">
            </div>
             <div class="form-group col-md-6 input-group" style="padding: 0px 15px !important;">
                <span class="input-group-addon" style="border: 1px solid;">@</span>
                <input type="text" class="form-control" name="fb" maxlength="50" placeholder="Facebook ID" style="border-left: 0px;">
              </div>
            <div class="form-group col-md-12">
            <label for="photo">Your photo :</label><input class="form-control" type="file" name="photo" id="photo" value="photo" required onchange="validateFiles(this);">
            <span class="error"></span>
            </div>
            <div class="form-group col-md-12 conta contact-popup">
                <span class="msg-error" style="color: red; float: right;"></span>
                <div class="g-recaptcha" id="recaptcha1"></div>
            </div>
            <div class="form-group col-md-12 text-right magZero">
                <input type="submit" class="submit-btn bg-black" name="submit" id="btnmodel" value="Submit">
            </div>
        </form>
        
    </div>

    </div>
</div>
</div>
</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113893312-1"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
 <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<script type="text/javascript">



$('#dob').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        maxDate:moment(new Date()),
        time:false

    });

/*$( '#btnmodel' ).click(function(){
  var $captcha = $( '#recaptcha' ),
      response = grecaptcha.getResponse();
  
  if (response.length === 0) {
    $( '.msg-error').text( "reCAPTCHA is mandatory" );
    if( !$captcha.hasClass( "error" ) ){
      $captcha.addClass( "error" );
    }
  } else {
    $( '.msg-error' ).text('');
    $captcha.removeClass( "error" );
    alert( 'reCAPTCHA marked' );
  }
})*/

function validateFiles(inputFile) { 
 var maxExceededMessage = "This file exceeds the maximum allowed file size (5 MB)";
  var extErrorMessage = "Only file with extension: .jpeg .png .jpg is allowed";
  var allowedExtension = ["jpeg", "png", "jpg"];

  var extName;
  var maxFileSize = 5242880;
  var sizeExceeded = false;
  var extError = false;

  $.each(inputFile.files, function() {
  if(this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
    extName = this.name.split('.').pop();
    if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
  });
  if (sizeExceeded) {
    $(".error").text(maxExceededMessage);
    $(inputFile).val('');
  };

  if (extError) {
    $(".error").text(extErrorMessage);
    $(inputFile).val('');
  };
}
</script>

