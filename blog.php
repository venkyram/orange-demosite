<!DOCTYPE html>
<html>
<head>
	<title>Blog | Orange Salons</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <meta property="og:title" content="Blog | Orange Salon"/>
    <meta property="og:site_name" content="Orange Premium Discount Salon"/> 
    <meta propety="og:url" content="https://orangesalons.com/blog.php"/>
    <meta property="og:image" content="https://orangesalons.com/images/og-image.jpg"/>
    <meta property="og:description" content=" Blog Of Orange Salon">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@TheOrangeSalons">
    <meta name="twitter:title" content="Blog | Orange Salon"/>
    <meta name="twitter:image" content="https://orangesalons.com/images/og-image.jpg"/>
    <meta name="twitter:description" content="Blog Of Orange Blog">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'a209d1626e7e76fbc783c2240add73e2b30afceb');
</script>
</head>
<body>
<?php include_once("header-secondary.php"); ?>
 <?php include_once("book-appointment.php"); ?>
<div class="phone-contact1">
       <span class="phone-icon"><img src="images/contact.svg"></span>
       <span class="phone-no"><a href="tel:+918807088076">+91 8807088076</a></span>
     </div>
<div class="header-offset"></div>
<section class="container-fluid blog-top flex-content">
	<div class="container">
		<div class="col-md-12">
			<h1>Our Blog</h1>
		</div>
	</div>
</section>
<section class="container-fluid blog-list">
	<div class="container">
		<div class="col-md-12 blogs-all">
			<div class="col-md-4 col-xs-12">
				<div class="blog-page">
					<div class="blog-img">
						<img src="images/page-blog1.jpg" class="img-responsive">
					</div>
					<div class="blog-content">
					<h3> Acne skin problems</h3>
					
					<p class="author magZero">
						Kothai
					</p>
					<p class="blog-date magZero">
						Jan 17, 2018
					</p>
					<p class="blog-details">Everyone wishes to have a clean and clear skin, but most of you fail to understand the proper working mechanism of your body & skin and the various glands that are present beneath.</p>
					</div>
					<a href="Acne-skin-problem.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="blog-page">
					<div class="blog-img">
						<img src="images/page-blog2.jpg" class="img-responsive">
					</div>
					<div class="blog-content">
					<h3>Open Pores</h3>
					
					<p class="author magZero">
						Kothai
					</p>
					<p class="blog-date magZero">
						Jan 27, 2018
					</p>
					<p class="blog-details">The skin has many small holes for hair follicle openings that are barely visible to the eye, called pores. They are present all over the body, excluding the palms and sole. Mostly they can’t be seen but some people may have evident pores that are quite visible.</p>
					</div>
					<a href="open-pores.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
			<!-- <div class="col-md-4 col-xs-12">
				<div class="blog-page">
					<div class="blog-img">
						<img src="images/blog-1.jpg" class="img-responsive">
					</div>
					<div class="blog-content">
					<h3>Lorem lpsum is simply</h3>
					
					<p class="author magZero">
						muniyamma
					</p>
					<p class="blog-date magZero">
						july 11, 2018
					</p>
					<p class="blog-details">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					</div>
					<a href="blog-page.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div> -->
		</div>
		<!-- <div class="col-md-12 blogs-all">
			<div class="col-md-4 col-xs-12">
				<div class="blog-page">
					<div class="blog-img">
						<img src="images/blog-1.jpg" class="img-responsive">
					</div>
					<div class="blog-content">
					<h3>Lorem lpsum is simply</h3>
					
					<p class="author magZero">
						muniyamma
					</p>
					<p class="blog-date magZero">
						july 11, 2018
					</p> 
					<p class="blog-details">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					</div>
					<a href="blog-page.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div> -->
	</div>
</section>
<?php include_once("footer.php"); ?>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113893312-1"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript">
	window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113893312-1');

 
	
</script>


</html>
