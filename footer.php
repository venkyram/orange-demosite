 <div class="loader-wrapper">
  <img src="images/orange-loader.gif"/>
</div>
 <section class="container-fluid footer padZero">
    <div class="container padZero">
        <div class="col-md-3 col-xs-12 logo-wrapper padZero">
            <a href="/" class="footer-logo">
                  <img class="img-responsive" src="images/orange-logo.png"></a>
        </div>
        <div class="col-md-3 col-xs-12 padZero">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 text-right padZero">
            <nav class="footer-menu">
                <ul class="padZero magZero">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php" data-id="about" class="scroll-link">About Us </a></li>
                    <li><a href="index.php" data-id="services" class="scroll-link">Services</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php" data-id="faq" class="scroll-link">FAQ</a></li>
                    <li><a href="" data-toggle="modal" data-target="#model">Be a Model</a></li>
                    
                    <!-- <li><a href="contact.php">Contact Us</a></li> -->
                    <li><a href="" data-toggle="modal" data-target="#careers">Careers</a></li>
                </ul>
            </nav>
            <ul class="social-icon text-right magZero">
                <li><a href="https://www.facebook.com/pg/TheOrangeSalons" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/TheOrangeSalons" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/theorangesalons/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <!-- <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
            </ul>
        </div>
        <div class="col-md-12 padZero">
            <div class="col-md-6 col-xs-12  address-div padZero">
            <div class="col-md-6 padZero">
                <div class=" col-md-12 col-xs-12 office-address padZero" >
                <h5>Salon Address</h5>
            </div>
            <div class=" col-md-12 col-xs-12 padZero align-vertical">
            <a href="https://maps.google.com?saddr=Current+Location&daddr=13.115431,80.102970" target="_blank" class="footer-map">
                <div class="col-md-1 padZero">
                     <img src="images/map-pin.png">
                </div>
                <div class="col-md-11 address">
                    
                    <p>
                       No.391, New Military Road,</br>
                        Thirumalairajapuram,TNHB,</br>
                        Avadi, Chennai - 600054 
                    </p>
                </div>
            </a>
            </div>
            </div>
            <div class="col-md-6 col-xs-12 padZero">
                <div class="col-md-12  col-xs-12 saloon_address padZero">
                <h5>Corporate Address</h5>
            </div>
           <div class=" col-md-12 col-xs-12  align-vertical padZero">
            <a href="https://maps.google.com?saddr=Current+Location&daddr=13.124779,80.026077" target="_blank" class="footer-map">
                <div class="col-md-1 padZero">
                    <img src="images/map-pin.png">
                </div>

                <div class="col-md-11 address">
                    <p>No.390, C.T.H Road,</br>
                    Thiruninravur,</br>
                Chennai - 602024</p>
                </div>
             </a>
            </div>
            </div>
        </div>
        </div>

    </section>
    <section class="container-fluid footer-copyright">
        <div class="col-md-4 padZero text-left">Owned by Hairport Aesthetics Pvt Ltd.</div>
        <div class="col-md-4 col-xs-12 text-center">
            Orange &copy; 2018 All rights reserved
        </div>

        <div class="col-md-4 col-xs-12 text-right createby">
        <p class="magZero"><a href="https://digiryte.com" target="_blank">Powered by Digiryte</a></p>
        </div>
    </section>


    <div id="careers" class="modal fade" role="dialog">
                <div class="modal-dialog ">
                    <!-- Modal content-->
                    <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="modal-body">
                            <h3 class="modal-title">Careers</h3>
                            <form role="form" method="post" id="careers" >
                                <span id="spnPhoneNoCareer"></span>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" id="" name="Name" required maxlength="50" placeholder="Name">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" id="phone_career" name="phone" required maxlength="10" placeholder="Mobile Number">
                                </div>
                                <div class="form-group col-md-12">

                                    <input type="text" class="form-control" id="emp" name="email" required maxlength="50" placeholder="Current/Previous place of Employment">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" id="exp" name="exp" required maxlength="50" placeholder="Year of experience">
                                </div>

                                <div class="form-group col-md-12 ">
                                    <textarea name="can_do" placeholder="Additional info" class="form-control"></textarea>
                                </div>
                                <div class="form-group col-md-12 conta contact-popup">
                                <div class="g-recaptcha" id="recaptcha"></div>
                                  </div>
                                <div class="form-group col-md-12 text-right magZero">
                                <button type="submit" class="submit-btn bg-black" id="btnContactUs">Submit</button>
                            </div>


                            </form>
                            
                        </div>
                    </div>
                </div>
    </div>

            <?php include_once("be-a-model.php"); ?>


            <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

