<!DOCTYPE html>
<html>

<head>
    <title>Home | Orange Salons</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <meta property="og:title" content="Orange Salons" />
    <meta property="og:site_name" content="Orange Premium Discount Salon" />
    <meta propety="og:url" content="https://orangesalons.com/" />
    <meta property="og:image" content="https://orangesalons.com/images/og-image.jpg" />
    <meta property="og:description" content="Orange is your exquisite neighbourhood haven of styling and pampering. We carry the finest beauty products and modish stylists to offer the largest selection of beauty services in the city. You get the extraordinary bit of serendipity in availing discounts on every visit. Orange promises to make premium services highly affordable, now & forever.">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@TheOrangeSalons">
    <meta name="twitter:title" content="Orange Salons" />
    <meta name="twitter:image" content="https://orangesalons.com/images/og-image.jpg" />
    <meta name="twitter:description" content="Orange is your exquisite neighbourhood haven of styling and pampering. We carry the finest beauty products and modish stylists to offer the largest selection of beauty services in the city. You get the extraordinary bit of serendipity in availing discounts on every visit. Orange promises to make premium services highly affordable, now & forever.">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="css/colorbox.css">
    <link rel="stylesheet" type="text/css" href="skin/light.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
    <script type="text/javascript">
        window.smartlook || (function(d) {
            var o = smartlook = function() {
                    o.api.push(arguments)
                },
                h = d.getElementsByTagName('head')[0];
            var c = d.createElement('script');
            o.api = new Array();
            c.async = true;
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.src = 'https://rec.smartlook.com/recorder.js';
            h.appendChild(c);
        })(document);
        smartlook('init', 'a209d1626e7e76fbc783c2240add73e2b30afceb');
    </script>
    
</head>

<body>
    <?php include_once("header-primary.php"); ?>
        <?php include_once("book-appointment.php"); ?>

            <section class="container-fluid landing-page padZero flex-content">
                <img src="images/circles.svg" class="img-responsive landing-circle hidden-sm hidden-xs">
                <div class="phone-contact">
                    <span class="phone-icon"><img src="images/contact.svg"></span>
                    <span class="phone-no"><a href="tel:+918807088076">+91 8807088076</a></span>
                </div>

                <div class="slider-offset">
                    <div class="container">
                        <div class="col-md-12 padZero">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="brand-text">
                                    <div>
                                        <h1>India's 1<sup>st</sup><br>
              <span class="highlighter">Premium</span><br>
                                   Discount Salon</h1>
                                        <h4>Women & Kids only</h4>
                                        <h5>Monday - Sunday: 09:30 AM - 08:30 PM</h5>
                                        <!-- <a href="" class=" bg-black orange-btn" data-toggle="modal" data-target="#book">Book Appointment</a> -->
                                        <img class="img-responsive mob-wheel visible-xs hidden-sm hidden-md hidden-lg" src="images/orange-wheel.png">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 orange-wheel hidden-xs col-sm-6">
                                <img class="img-responsive" data-wow-duration="2s" data-wow-iteration="infinite" src="images/orange-wheel.png">
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section class="container-fluid whoweare padZero align-vertical" id="about">

                <span class="layer1  hidden-xs hidden-sm" data-wow-duration="2s">
      <img src="images/whoweare-left.png" class="img-responsive">
         </span>
                <div class="container padZero">
                    <div class="col-md-4 col-md-offset-8 col-sm-12 padRight">
                        <div class="whoweare-content">
                            <h1>Who We Are?</h1>
                            <p>Orange is your exquisite neighbourhood haven of styling and pampering. We carry the finest beauty products and modish stylists to offer the largest selection of beauty services in the city. You get the extraordinary bit of serendipity in availing discounts on every visit. Orange promises to make premium services highly affordable, now & forever. </p>
                        </div>
                        <div>
                            <span id="custom"></span>

                        </div>
                    </div>
            </section>
            <section class="container-fluid services padZero hidden-xs hide-service" id="services">
                <span class="service-img"></span>

                <div class="container padZero">
                    <div class="col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 service-title padZero col-xs-6">
                        <h1>Our Services</h1>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center" style="padding-top: 10px;">

                        <div class="form-group input-group home-search">
                            <input type="input" class="form-control txt-search" id="txt-search" placeholder="Start typing your service..." autocomplete="off">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>

                    </div>

                    <div class="filter-records col-md-9 col-sm-9 col-sm-3-offset col-md-offset-3">

                    </div>
                    <div class="search-hide col-md-12 col-sm-12">
                        <div class="col-md-2 col-md-offset-3 col-sm-3 col-sm-offset-2 padZero list-service col-xs-12">
                            <h4>Category</h4>
                            <ul class="service-names padZero magZero">

                            </ul>
                        </div>
                        <div class="col-md-7 col-sm-7 padZero col-xs-12">
                            <div class="service-box">
                                <a href="contact.php" data-id="faq" class="faq-link scroll-link">Frequently Asked Questions</a>
                                <ul class="service-content">

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="container-fluid services padZero hidden-md hidden-lg mobile_top" id="ser-scroll">
                <div class="search-services">
                    <div class="close-btn text-right">
                        <i class="fa fa-times fa-2x"></i>
                    </div>
                    <div class="container box-width">
                        <form role="form">
                            <div class="form-group">
                                <input type="input" class="form-control input-lg txt-search" id="txt-search" placeholder="Start typing your service..." autocomplete="off">
                            </div>
                        </form>
                        <div class="filter-records"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="service-title padZero col-xs-10">
                        <h1>Our Services</h1>
                    </div>
                    <div class="col-xs-2 search-pop text-center">
                        <i class="fa fa-search fa-2x"></i>
                    </div>
                    <div class="padZero list-servicemob col-xs-12">
                        <ul class="servicename-mobile">
                        </ul>
                    </div>
                    <div class=" service-mobile  padZero col-xs-12">
                        <img src="images/left.svg" class="backTo">
                        <ul class=" padZero magZero">
                        </ul>
                    </div>
                </div>
<div class="text-center" style="margin-top: 20px;">
                <a href="contact.php" data-id="faq" class="faq-moblink orange-btn bg-white scroll-link">FAQ</a>
            </div>
            </section>
            <section class="container-fluid products padZero  flex-content" id="brands">
                <div class="container text-center" data-wow-duration="2s">
                    <div class="product">
                        <img src="images/product-1.png" class="img-responsive">
                    </div>
                    <div class="product">
                        <img src="images/product-2.png" class="img-responsive">
                    </div>
                    <div class="product">
                        <img src="images/product-3.png" class="img-responsive">
                    </div>
                    <div class="product">
                        <img src="images/product-4.png" class="img-responsive">
                    </div>
                    <div class="product">
                        <img src="images/product-5.png" class="img-responsive">
                    </div>
                </div>
            </section>
            <!-- <section class="container-fluid gallery padZero flex-content">
        <div class="container padZero flex-content"> -->
            <!-- <div class="gallery-title col-md-6">
                <h1>Walk Through</h1>
            </div> -->
            <!-- <div class="images col-md-6">  -->

            <!-- <a href="https://www.google.com/maps/embed?pb=!4v1521483764235!6m8!1m7!1sCAoSK0FGMVFpcE1JTUJ5T2lYYkxLMlFYbTBYMW1ncXpaeHUxS2FjOGJMeHdKTGc.!2m2!1d13.11542951718336!2d80.10284718585865!3f313.11!4f-0.9599999999999937!5f0.7820865974627469" class="img-iframe">
            <div class="img elem"><img src="images/view.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="play">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!-- <a href="https://www.google.com/maps/embed?pb=!4v1522139355866!6m8!1m7!1sCAoSK0FGMVFpcFBNWTlWdHdSRWVXVWtOcUw1MHZVRGVwcG91bkVPOUxwVU5jQ1E.!2m2!1d13.11544191647227!2d80.10291174446456!3f196.2785752559918!4f11.29286638513733!5f0.7820865974627469" class="img-iframe">
            <div class="img elem"><img src="images/walkthrough_thumbnail.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="play">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!--  <a href="https://www.google.com/maps/embed?pb=!4v1521484170931!6m8!1m7!1sCAoSK0FGMVFpcE1kbEgyU3R0aFBjT2lVbHRHaEljY1BOQURFT2p1Z05md1VTTlk.!2m2!1d13.11544254653777!2d80.10276527508563!3f70.65!4f-26.93!5f0.7820865974627469" class="img-iframe">
            <div class="img elem"><img src="images/spa.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="play">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!-- <a href="https://www.google.com/maps/embed?pb=!4v1521484239611!6m8!1m7!1sCAoSK0FGMVFpcE5CNXMzTVFaakhuWTgyQm1XMlktSXJWUkc3TjNkTmM5RTRUX1U.!2m2!1d13.11544611203552!2d80.10273784706385!3f263.67!4f-3.549999999999997!5f0.7820865974627469" class="img-iframe">
            <div class="img elem"><img src="images/reception.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="play">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!-- <a href="https://www.google.com/maps/embed?pb=!4v1521484276855!6m8!1m7!1sCAoSK0FGMVFpcE1Yd1BOUWpCMldHLVpnRV82eUg3ZGtpQ2tKTlNrOC03TGVWTms.!2m2!1d13.11542951718336!2d80.10284718585865!3f90.68!4f-16.409999999999997!5f0.4000000000000002" class="img-iframe">
            <div class="img elem"><img src="images/view1.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="play">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!-- <a href="https://www.google.com/maps/embed?pb=!4v1521484339811!6m8!1m7!1sCAoSK0FGMVFpcE41emkxRUVQa2Z1eXdPR0x0ZHVQZktXVFFMWjRuS0NiRVpEb3c.!2m2!1d13.11548373996192!2d80.10272718156602!3f98.4!4f-10.819999999999993!5f0.7820865974627469" class="img-iframe">
            <div class="img elem"><img src="images/view3.jpg"> </div>
            <div class="overlay">
              <div class="icon" title="User Profile">
                 <i class="fa fa-play"></i>
              </div>
            </div>
        </a> -->
            <!-- </div> -->
            <!-- div class="text-center col-md-12 col-xs-12 button-btn">
                <a href="gallery.php" class="orange-btn bg-white">View More</a>
            </div>
        </div> -->
            <!--   </section> -->

            <section class="container-fluid testimonial-block flex-content">
                <div class="container padZero">
                    <div>
                        <img src="images/logo-o.png" class="img-responsive logo-o">
                    </div>
                    <div class="test-title">
                        <h1>Happy Guests</h1>
                    </div>

                    <div class="col-md-offset-2 col-md-8">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>Dear Orange, Awesome interiors.!! The name which suits it.!!
                                                <br>We Indians are known for hospitality and u guys proved it.!! I wish that u guys should continue with the same service and hospitality.. Really impressed..!! Yes it's having both the components of Premium and Discount.</p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Bhavani Sundaramoorthi</h3>
                                            <img src="images/stars.png">

                                        </div>
                                    </div>
                                </div>

                                <div class="item">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>Graceful ambience and courteous staff. Overall an IMPRESSIVE lounge at avadi!!
                                                <br>And not to forget to mention the spin-wheel concept, brings the child in us out.
                                                <br>Great job guys keep going <i class="em em---1"></i></p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Sangeetha Nageswara rao</h3>
                                            <img src="images/stars.png">
                                        </div>
                                    </div>
                                </div>

                                <div class="item">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>Lovely atmosphere in salon, first visit and really impressed by the friendly and professional service . Great suggestions for a new style <i class="em em---1"></i> <i class="em em---1"></i></p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Kokila Ramaninatarajan</h3>
                                            <img src="images/stars.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>I'm very much impressed by the way they present the services. When the wheel spins my heart pumps and to my surprise I got 40%.</p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Shalini</h3>
                                            <img src="images/stars.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>The massage after the facial was the best. I loved the overall experience.</p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Namrutha</h3>
                                            <img src="images/stars.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">

                                    <div class="testimonial">
                                        <div>
                                            <img src="images/quotes.png" class="img-responsive">
                                        </div>
                                        <div class="testimonial-content">
                                            <p>Booked MAC make up for my wedding at orange and the staff by name Jeni has done a nice make up for me. I'm very happy.</p>
                                        </div>
                                        <hr align="center" width="40%">
                                        <div class="reviews text-center">
                                            <h3>Reena</h3>
                                            <img src="images/stars.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                <li data-target="#myCarousel" data-slide-to="4"></li>
                                <li data-target="#myCarousel" data-slide-to="5"></li>
                            </ol>
                            <div class="arrows">
                                <a class="left-img" href="#myCarousel" data-slide="prev">
                                    <img src="images/left.png" class="img-responsive">
                                </a>
                                <a class="right-img" href="#myCarousel" data-slide="next">
                                    <img src="images/right.png" class="img-responsive">
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="container-fluid padZero blogs">
                <div class="container padZero">
                    <div class="blog-title">
                        <h4>Latest from</h4>
                        <h1>Our Blogs</h1>
                    </div>
                    <span class="blog-box hidden-xs"></span>
                    <div class="col-md-12 padZero ">
                        <div class="col-md-6 blog m-top-30">
                            <div class="blog-detail col-md-6 col-xs-6 text-right">
                                <div class="heading-blog">
                                    <h4>Acne skin problem</h4>
                                </div>
                                <p class="blog-date">
                                    Jan 27,2018
                                </p>
                                <!--  <p class="blog-text">
                           Everyone wishes to have a clean and clear skin, but most of you fail to understand the proper working mechanism of your body & skin and the various glands that are present beneath.
                        </p> -->
                                <p class="author">
                                    Kothai
                                </p>
                                <a href="Acne-skin-problem.php" class="blog-link-left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Read More </a>
                            </div>
                            <div class="blog-img col-md-6 col-xs-6 padRight">
                                <img src="images/home-blog2.jpg" class="img-responsive">
                            </div>
                        </div>
                        <!-- <div class="col-md-6 blog hide" data-wow-delay="2s">
                    <div class="blog-img col-md-6 padLeft">
                        <img src="images/blog-2.jpg" class="img-responsive">
                    </div>
                    <div class="blog-detail col-md-6 text-left">
                        <p class="blog-date">
                            July 11, 2018
                        </p>
                        <p class="blog-text">
                            Lorem Ipsum has been the industry's standard dummy text 
                        </p>
                        <p class="author">
                            Monisha Patel
                        </p>
                        <a href="blog-page.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div> -->
                    </div>
                    <div class="col-md-12 padZero">
                        <!-- <div class="col-md-6 blog hide" data-wow-delay="2s">
                    <div class="blog-detail col-md-6  text-right">
                        <p class="blog-date">
                            July 11, 2018
                        </p>
                        <p class="blog-text">
                            Lorem Ipsum has been the industry's standard dummy text 
                        </p>
                        <p class="author">
                            Monisha Patel
                        </p>
                        <a href="blog-page.php" class="blog-link-left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Read More </a>
                    </div>
                    <div class="blog-img col-md-6 padRight">
                        <img src="images/blog-3.jpg" class="img-responsive">
                    </div>
                </div> -->
                        <div class="col-md-6 blog col-md-offset-6 m-top-45-minus">
                            <div class="blog-img col-md-6 padLeft ">
                                <img src="images/home-blog1.jpg" class="img-responsive">
                            </div>
                            <div class="blog-detail col-md-6 col-xs-6 text-left">
                                <div class="heading-blog">
                                    <h4>Open Pores</h4>
                                </div>
                                <p class="blog-date">
                                    Jan 27, 2018
                                </p>
                                <!-- <p class="blog-text">
                            The skin has many small holes for hair follicle openings that are barely visible to the eye, called pores. They are present all over the body, excluding the palms and sole. 
                        </p> -->
                                <p class="author">
                                    Kothai
                                </p>
                                <a href="open-pores.php" class="blog-link">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="text-center col-md-12 col-xs-12 button-btn">
                        <a href="blog.php" class="orange-btn bg-white">View More</a>
                    </div>
                </div>
            </section>
            <section class="container-fluid contact" id="contact">
                <div class="container">
                    <div class="contact-title text-center">
                        <h4>Contact Us</h4>
                        <h1>Get in Touch</h1>
                    </div>
                    <div class="col-md-offset-6 col-md-6 padZero">
                        <div class="contact-details">
                            <p><img src="images/phone-icon.png"><a href="tel:+918807088076">+91 88070 88076</a></p>
                            <p><img src="images/mail-icon.png"><a href="mailto:contact@orangesalons.com" target="_blank"> contact@orangesalons.com</a></p>
                        </div>
                    </div>
            </section>
            <?php include_once("footer.php"); ?>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113893312-1"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->

<script type="text/javascript">
    $(document).ready(function() {

        /*lc_lightbox('.elem', {
                wrap_class: 'lcl_fade_oc',
                gallery : true, 
                thumb_attr: 'data-lcl-thumb', 
                skin: 'light',
                radius: 0,
                padding : 0,
                border_w: 0,
            }); */

        $(".images a").colorbox({
            iframe: true,
            width: "99%",
            height: "100%",
            rel: "gel",
            fixed: true
        });

        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-113893312-1');

    });
</script>

</html>