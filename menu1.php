<!DOCTYPE html>
<html>

<head>
    <title>Orange</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="js/tag-it.js"></script>
    <script type="text/javascript" src="js/cookies.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    $('ul.service-names li').click(function(){
      $(this).addClass("active");

    });
     $("#searchmenu").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    var b=[];
    $(".service-names1 li").each(function() {
     var text = $(this).text().toLowerCase();
     var match = text.indexOf(value)
     if (match >= 0) 
            {
                b.push(this);
                $(this).show();
                b[0].click();
            }
            else
            {
                $(this).hide();
            }
           

   });
    console.log(b)
  });

    });
    </script>
</head>
<body data-spy="scroll" data-target="#myScroll"  >
  <header>
        <div class="navbar container-fluid sticky-one align-vertical">
            <div class="col-md-2 col-xs-10 logo-wrapper">
                <a href="/" class="logo"><img class="img-responsive" src="images/orange-logo.png"></a>
            </div>
            <div class="col-md-10 col-sm-12 text-right hidden-xs visible-lg visible-md hidden-sm padZero">
            </div>
            <div class=" visible-xs hidden-lg hidden-md visible-sm mobile-menu-wrapper
            ">
            </div>
        </div>
    </header>
    <div class="header-offset"></div>

  <div class="container padZero">
    <div class="row">
      <div class="col-md-10 service-title padZero col-xs-12">
          <h1>Our Services</h1>
      </div>
    </div>
    <div class="row">

      <nav class="col-md-2 col-md-offset-1 padZero col-xs-12" id="myScroll" style="color: #000">
       <div class="input-group" style="margin-bottom: 10px;">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                <input id="searchmenu" class="search form-control" placeholder="Search" />
                
                    </div>
          <ul class="service-names1 padZero magZero nav nav-stacked">

          </ul>
          <!-- <ul id="pagin">
            <li><a class="current" href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            
         </ul> -->
        </nav>
        
         <!--  <a class="down trigger-scroll"><img src="images/down.svg"></a> -->
      
      <div class="col-md-7 padZero col-xs-12">
          <div class="service-bo">
              <ul class="service-content1" >

              </ul>
          </div>
      </div>
      </div>
    </div>
</div>
</body>
</html>