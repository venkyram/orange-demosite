<!DOCTYPE html>
<html>
<head>
    <title>Franchise | Orange Salons </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <meta property="og:title" content="Franchise | Orange Salons"/>
    <meta property="og:site_name" content="Orange Premium Discount Salon"/> 
    <meta propety="og:url" content="https://orangesalons.com/franchise.php"/>
    <meta property="og:image" content="https://orangesalons.com/images/franshice-og.png"/>
    <meta property="og:description" content="Beauty yields Big Come, let's fly together on our successful business journey.">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@TheOrangeSalons">
    <meta name="twitter:title" content="Franchise | Orange Salons"/>
    <meta name="twitter:image" content="https://orangesalons.com/images/franshice-og.png"/>
    <meta name="twitter:description" content="Beauty yields Big Come, let's fly together on our successful business journey.">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'a209d1626e7e76fbc783c2240add73e2b30afceb');
    </script>
</head>
<body>
    <?php include_once("header-secondary.php"); ?>
     <?php include_once("book-appointment.php"); ?>
<div class="phone-contact1">
       <span class="phone-icon"><img src="images/contact.svg"></span>
       <span class="phone-no"><a href="tel:+918807088076">+91 8807088076</a></span>
</div>
   

    <div class="header-offset"></div>
    <section class="container-fluid franchise-top flex-content">
        <div class="container">
        <div class="col-md-12">
            <div class="franchise-title">
                <h1>Interested in Franchise?</h1>
                <h4>Fill in the form below</h4>
            </div>
            </div>
        </div>
    </section>
    <section class="container-fluid contact-form">
    <div class="container">
        <div id="loading-image" style="display: none;">
                    <img src="images/orange-loader.gif">
                </div>
        <div class="col-md-12">
            <div class="form-container">
              <div class='alert success' style="display:none;">
                Thanks for getting in touch. We will get back to you shortly.
              </div>
              <div class='alert error' style="display:none;">
                Sorry! Your form wasn't submitted. Kindly try again.
              </div>
    <form role="form" method="POST"  class="formsubmit">
        <span id="spnPhoneNo"></span> 
          <input type="hidden" name="form_type" id="form_type" value="franchise">
        <div class="form-group">
            <input type="text" name="contact_name" id="contact_name" placeholder="Name" class="form-control" maxlength="50" required="" aria-required="true">
        </div>
        <div class="form-group input-group">
            <span class="input-group-addon">+91</span>
            <input type="text" name="primary_phone" id="primary_phone" placeholder="Primary Phone No" class="form-control" required="" aria-required="true" maxlength="10">
        </div>
        <div class="form-group input-group">
            <span class="input-group-addon">+91</span>
            <input type="text" name="secondary_phone" id="secondary_phone" placeholder="Secondary Phone No" class="form-control"  maxlength="10">
        </div>
        <div class="form-group">
            <input type="email" name="contact_email" id="contact_email" placeholder="Email Address" class="form-control" required="" aria-required="true">
        </div>
        <div class="form-group">
            <textarea name="can_do" id="can_do" placeholder="Additional info" class="form-control" ></textarea>
        </div>
        <div class="form-group">
             <span class="msg-error error" style="color:red"></span>
            <div class="g-recaptcha" id="recaptcha2" ></div>
        </div>
        <div class=" col-md-12 m-top-20 padZero">
            <input type="submit" class="contact-btn bg-white" value="Submit">
        </div>
    </form>
        </div>
        </div>
    </div>
</section>
    <?php include_once("footer.php"); ?>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="js/tag-it.js"></script>
    <script type="text/javascript" src="js/cookies.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113893312-1"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113893312-1"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript">
     $(document).ready(function() {
    $('ul.service-names li').click(function(){
     $(this).addClass("active");

    });

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-113893312-1');
});
</script>


</html>



