<!DOCTYPE html>
<html>
<head>
	<title> Feedback | Orange Salon </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
</head>
<body>
<section class="container-fluid flex-content feedback-page">
	<div class="container flex-content">
		<div class="col-md-5 col-sm-5 flex-content col-xs-12">
			<img src="images/orange-0.png" class="img-responsive">
		</div>
		<div class="col-md-7 col-sm-7 col-xs-12">
			<div class="col-sm-12 col-md-12 ">
			<a href="menu.php"><div class="box-shadow align-vertical">
				<div class="col-md-2 col-sm-4 padZero">
					<img src="images/menu.svg" class="img-responsive">
				</div>
				<div class="col-sm-8 col-md-10">
					<p>MENU CARD</p>
				</div>
			</div></a>
			</div>
			<div class="col-sm-12 col-md-12">
				<a href="http://store.orangesalons.com/feedbacks">
			<div class="box-shadow align-vertical">
				<div class="col-md-2 col-sm-4 padZero">
					<img src="images/feedback.svg" class="img-responsive">
				</div>
				<div class="col-sm-8 col-md-10">
					<P>RECORD MY FEEDBACK</P>
				</div>
			</div></a>
			</div>
			<div class="col-sm-12 col-md-12">
				<a href="video.php">
				<div class="box-shadow align-vertical">
				<div class="col-md-2 col-sm-4 padZero">
					<img src="images/Video.svg" class="img-responsive">
				</div>
				<div class="col-sm-8 col-md-10">
					<P>SPINWHEEL EXPLAINER</P>
				</div>
			</div></a>
		</div>
		</div>
	</div>
</section>
</body>
</html>