<!DOCTYPE html>
<html>

<head>
    <title>Contact | Orange Salons</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="images/orange_fav.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom-style.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">
</head>

<body>
    <?php include_once("header-secondary.php"); ?>
    
    <div class="phone-contact1">
       <span class="phone-icon"><img src="images/contact.svg"></span>
       <span class="phone-no"><a href="tel:+918807088076">+91 8807088076</a></span>
     </div>
    <div class="header-offset"></div>
    <section class="container-fluid contact-top flex-content">
        <div class="container">
        <div class="col-md-6 col-md-offset-4 padZero">
            <div class="contact-head text-center">
                <!-- <h4>Contact Us</h4> -->
                <h1>Get in Touch</h1>
            </div>
            <div class="col-md-offset-6 col-md-6 padZero">
                <div class="contact-p">
                    <p><a href="tel:8807088076"><img src="images/call.png">+91 88070 88076</a></p>
                    <p><a href="mailto:contact@orangesalons.com"><img src="images/mail.png">contact@orangesalons.com</a></p>
                </div>
            </div>
            </div>
        </div>
    </section>
    <section class="container-fluid faq" id="faq">
        <div class="container">
            <div class="faq-title text-center col-md-12">
                <h1>FAQ</h1>
            </div>
             <div class="col-md-12 padZero">
            <ul class="nav nav-tabs nav-justified" role="tablist" id="faq">
                <li role="presentation" class="active border">
                <a href="#one" role="tab" data-toggle="tab" class="">Appointment Booking</a>
                </li>
                <li role="presentation" class="border">
                <a href="#two" role="tab" data-toggle="tab" class="">Service Related</a>
                </li>
                <li role="presentation" class="">
                <a href="#three" role="tab" data-toggle="tab" class="mob-bor">Spin Wheel</a>
                </li>
            </ul>
<div class="tab-content ">
 <div id="one" role="tabpanel" class="tab-pane fade in active">
<div class="main">
    <div class="accordion">
        <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-1"> 
          How do I book a service?
        </a>
        <div id="accordion-1" class="accordion-section-content">
        <p>You can book with us by using our booking page here on our website. Alternatively, you can call us on 8807088076 to book your appointment.</p>
      </div>
      </div>
    
  <div class="accordion-section">
      <a class="accordion-section-title" href="#accordion-2"> 
          How do I reschedule my appointment?
        </a>
    <div id="accordion-2" class="accordion-section-content">
        <p>You can contact us on <a href="tel:8807088076" class="faq-tel">8807088076</a>  to reschedule your appointment.</p>
      </div>
    </div>
</div>
</div>
</div>
  
<div id="two" role="tabpanel" class="tab-pane fade">
 <div class="main">
    <div class="accordion">
        <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-3"> 
          Do you provide consultations?
        </a>
    <div id="accordion-3" class="accordion-section-content">
        <p>Yes! A complimentary consultation concerning the needs of each individual is conducted before every service.</p>
      </div>
    </div>

       <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-4"> 
          How often should I get a facial?
        </a>

    <div id="accordion-4" class="accordion-section-content">
        <p>The recommended interval between any facial is 3-4 weeks.</p>
      </div>
    </div>

        <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-5"> 
           How often should I have a Manicure and Pedicure?
        </a>
    <div id="accordion-5" class="accordion-section-content">
        For healthy hands and nails you should have your manicure done once every 3 weeks and for pedicure once in every month. 
      </div>
    </div>

        <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-6"> 
         What is Rica Wax?
        </a>
    <div id="accordion-6" class="accordion-section-content">
        <p>Rica wax is also known as Liposoluble wax. Rica uses vegetable oil mixed with glyceryl rosinate which ensures painless removal of hair. Rica is completely free from skin sensitiser which is responsible for causing allergic dermatitis.</p>
      </div>
    </div>
 
     <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-7"> 
        How often should I wax? 
        </a>
      
    <div id="accordion-7" class="accordion-section-content">
       <p> Everyone’s hair growth is different. Still, waxing every 4-5 weeks will keep your skin soft, gentle and nourished.</p>
      </div>
    </div>
  
     <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-8"> 
       What colour line do you use?
        </a>
      
    <div id="accordion-8" class="accordion-section-content">
       <p> At Orange, we use L’Oréal Paris Professional colour.</p>
      </div>
    </div>
  
  <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-9"> 
      What is Smoothening?
        </a>
     
    <div id="accordion-9" class="accordion-section-content">
        <p>Hair smoothening is a smarter way to keep your hair frizz-free & make it look straight & silky. You will be getting more natural look when compared to other hair treatments. This a temporary fix that lasts for 3 to 4 months & involves relatively less chemicals. It is a great option for treating dull hair, split ends and also eliminates frizziness by adding necessary moisture to it.</p>
      </div>
    </div>
  
  
  <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-10">
     What is Straightening?
        </a>
    <div id="accordion-10" class="accordion-section-content">
        Hair straightening is a process by which, the structure of your hair is altered to give you straight hair.  The result lasts long when compared to smoothening. 
      </div>
    </div>
 
  <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-11">
     What is Rebonding?
        </a>
     
    <div id="accordion-11" class="accordion-section-content">
        Rebonding is a permanent treatment to straight your hair. Your hair will remain straight and you can’t change your hair, the post treatment. You can only cut it, if you change your mind later. As Rebonding changes the hair structure, it makes the new hair growth more noticeable. So you have to re-do your hair treatment, as new untreated hair grows in.
      </div>
    </div>
    <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-12">
    What is Trial Makeup? 
        </a>
     
   <div id="accordion-12" class="accordion-section-content">
        A trial make up is usually recommended for brides before the BIG DAY. This makeup covers one side of the face to ensure that, you are happy with makeup artist. This session helps us in knowing your best features to be highlighted on the day of your wedding. You may also bring reference photos with you.
      </div>
    </div>
 </div>
 </div>
</div>
<div id="three" role="tabpanel" class="tab-pane fade">
    <div class="main">
    <div class="accordion">
        <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-13"> 
    What do the spin wheel do in orange?
        </a>
     
    <div id="accordion-13" class="accordion-section-content">
        As our name goes with Premium Discount Salon, we offer a guaranteed minimum discount of 20% up to a maximum of 50% through the spin wheel on all services.
      </div>
    </div>
  
  <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-14"> 
    What are all the services that are eligible for spinning?
        </a>
      
   <div id="accordion-14" class="accordion-section-content">
        You are allowed to spin for any service that you avail in our salon. No minimum order amount is required to spin.
      </div>
    </div>
 
  <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-15"> 
    How many times can I spin the wheel each day?
        </a>

     <div id="accordion-15" class="accordion-section-content">
        Any day, each customer on availing any service can spin the wheel once.
      </div>
    </div>

  <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-16"> 
   What to do with my discounts?
        </a>
      
    <div id="accordion-16" class="accordion-section-content">
        You can either avail the discounts personally on the same day and also for the next seven days. You may even share your available discounts among your friends.
      </div>
    </div>
 
  <div class="accordion-section">
            <a class="accordion-section-title" href="#accordion-17"> 
   Is there any validity for discounts?
        </a>
      
    <div id="accordion-17" class="accordion-section-content">
        Yes! The discount is valid up to seven days or seven services, whichever limit is reached first.
      </div>
    </div>
  
     <div class="accordion-section">
    <a class="accordion-section-title" href="#accordion-18"> 
   Can I spin for others?
        </a>
    <div id="accordion-18" class="accordion-section-content">
        No! You cannot spin for others. You are allowed to spin only if you take up any service in our salon.
      </div>
    </div>
  
  <div class="accordion-section">
    <a class="accordion-section-title" href="#accordion-19">
   What are the terms and conditions of spin wheel discounts?
        </a>
      
    <div id="accordion-19" class="accordion-section-content">
    Only the customer who avails a service in Orange salon is allowed to spin.
    Each customer can spin only once a day. The discount is valid for 7 days or first 7 services, whichever limit is reached first.

      </div>
    </div>
  </div>

    </div>

    </div>
</div>

    </div>
</div>
</section>
    <section class="container-fluid map">
    <div class="container "> 
        <div id="map">
            
        </div>
    </div>
    </section>
    
    <?php include_once("footer.php"); ?>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tag-it.js"></script>
<script type="text/javascript" src="js/cookies.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/custom-script.js"></script>
<script>
  var infowindows=[];
function addMarkerWithInfowindow(map, marker_position, infowindow_content){
              var myLatlng, marker, infowindow,contentString;
              
              marker = new google.maps.Marker({
                  position: marker_position,
                  map: map
              });
              contentString = infowindow_content;
              infowindow = new google.maps.InfoWindow({
                  content: contentString
              });
               infowindows.push(infowindow);

              marker.addListener('click', function() {
                closeInfoWindows();
                 infowindow.open(map, marker);

              });
         }
         function closeInfoWindows()
         {
          for (var i = 0; i < infowindows.length; i++) {
          infowindows[i].close();
        }
         }

         function initMap() {

          // init map
          var mapDiv = document.getElementById('map');
          myLatlng = new google.maps.LatLng(13.1249667,80.100960);
          var map = new google.maps.Map(mapDiv, {
            scrollwheel: false,
            center: myLatlng,
            zoom: 11
          });


          addMarkerWithInfowindow(map, new google.maps.LatLng(13.115431,80.102970), '<h4>Orange Salon(Avadi)</h3><div>391,New Military Road</div><div>Thirumalairajapuram,TNHB,</div><div>Avadi,Tamil Nadu 600054</div><div><a href="https://maps.google.com?saddr=Current+Location&daddr=13.115431,80.102970" target="_blank">Get Direction</a></div>');
          addMarkerWithInfowindow(map, new google.maps.LatLng(13.124779,80.026077), '<h4>Orange Salon(Thiruninravur)</h3><div>No,390,C.T.H Road,</div><div>Thiruninravur -602014</div><div><a href="https://maps.google.com?saddr=Current+Location&daddr=13.124779,80.026077" target="_blank">Get Direction</a></div>');


         }

         
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA09EOpz3wAqYOM6W6XpJw5WcIRyzCuNOM&callback=initMap">
    </script>

<script type="text/javascript">
    jQuery(document).ready(function() {
    function close_accordion_section() {
        jQuery('.accordion .accordion-section-title').removeClass('active');
        jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = jQuery(this).attr('href');

        if(jQuery(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();

            // Add active class to section title
            jQuery(this).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        }

        e.preventDefault();
    });
});
</script>

</html>
